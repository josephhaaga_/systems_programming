// Group 14 High Altitude Heater Part 2 
// Dan Coen, Max Friedman &  Joseph Haaga
// 1/2/16

// Inputs (potentiometers)
// Red: A0
// Green: A2
// Blue: A1

// Outputs (LED pins)
// Red: 3
// Green: 5
// Blue: 6

// Analog Pins A0, A1, and A2 read from the potentiometers
int REDsensorPin = A0;    // select the input pins for the potentiometer
int GREENsensorPin = A2;
int BLUEsensorPin = A1;    
int totSensorValue = 0;
int temperature = 0;
int pinVal = 0;
// set LED common anode pin
int ledPin = 11;
int REDsensorValue = 0;    // initialize pot values
int GREENsensorValue = 0;
int BLUEsensorValue = 0;
#define REDPIN 3
#define GREENPIN 5
#define BLUEPIN 6

void setup() {
  // initialize serial communications
  Serial.begin(9600);
  // set LED-driving pins to output (R=3, G=5, B=6)
  pinMode(ledPin, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  // Send 5V to LED common anode 
  digitalWrite(ledPin, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  // read the value from the sensor:
  REDsensorValue = analogRead(REDsensorPin);
  GREENsensorValue = analogRead(GREENsensorPin);
  BLUEsensorValue = analogRead(BLUEsensorPin);
  totSensorValue = REDsensorValue + GREENsensorValue + BLUEsensorValue;
  // calculate analog voltages from potentiometers via A0, A1 and A2
  float RedVoltage = REDsensorValue * (5.0 / 1023.0);
  float GreenVoltage = GREENsensorValue * (5.0 / 1023.0);
  float BlueVoltage = BLUEsensorValue * (5.0 / 1023.0);
  // we don't know why we did this
  float totalVoltage = RedVoltage + BlueVoltage + GreenVoltage;
  // Printing voltages for debug
  Serial.print("\n");
  Serial.print(RedVoltage);
  Serial.print("\t");
  Serial.print(GreenVoltage);
  Serial.print("\t");
  Serial.print(BlueVoltage);
  Serial.print("\n");
  analogWrite(GREENPIN,255-GREENsensorValue);
  analogWrite(REDPIN,255-REDsensorValue);
  analogWrite(BLUEPIN,255-BLUEsensorValue);

  // simplified LED color logic
  if(REDsensorValue>200){Serial.print("led is red");}
  else if (BLUEsensorValue>200){Serial.print("led is blue");}
  else {Serial.print("led is green");}
  

 


  // Print voltage and temperature to Serial Monitor as per project spec
  Serial.print(totalVoltage);
  Serial.print(" V\n");
  if (totalVoltage == 750) {
    temperature = 25;
  } else {
    temperature = 25 + ((totalVoltage - 750) / 10);
  }
  //Printing sensor values for debug
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.print("Celsius\n");
  Serial.print("\n");
  Serial.print(REDsensorValue);
  Serial.print("\t");
  Serial.print(GREENsensorValue);
  Serial.print("\t");
  Serial.print(BLUEsensorValue);
  Serial.print("\n");
  // turn the ledPin on
  delay(350);

}

