
sephhaaga@Josephs-MBP  ~/Google Drive/Systems_Programming/Homework1   master ●  ./a.out                                

Does the resistor have 4 or 5 bands?:
4
input:4

0. Black
1. Brown
2. Red
3. Orange
4. Yellow
5. Green
6. Blue
7. Violet
8. Gray
9. White
10. Gold
11. Silver

Please enter the number indicating the first color: 4

0. Black
1. Brown
2. Red
3. Orange
4. Yellow
5. Green
6. Blue
7. Violet
8. Gray
9. White
10. Gold
11. Silver

Please enter the number indicating the second color: 2

0. Black
1. Brown
2. Red
3. Orange
4. Yellow
5. Green
6. Blue
7. Violet
8. Gray
9. White
10. Gold
11. Silver

Please enter the number indicating the third color: 3

0. Black
1. Brown
2. Red
3. Orange
4. Yellow
5. Green
6. Blue
7. Violet
8. Gray
9. White
10. Gold
11. Silver

Please enter the number indicating the fourth color: 6


Resistor Calculator believes you have an

	 42 x 10^3 Ohm resistor with +/- 0.250000 percent tolerance
