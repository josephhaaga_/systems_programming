// Joseph Haaga Resistance Calculator for GWU CS3410 Systems Programming Spring 2016
#include <stdio.h>
#include <string.h>

// a helper function to print the number-color mappings
void printColorMap(){
	printf("\n0. Black\n1. Brown\n2. Red\n3. Orange\n4. Yellow\n5. Green\n6. Blue\n7. Violet\n8. Gray\n9. White\n10. Gold\n11. Silver\n");

}

// a helper function to keep track of how many numbers input thus far
int getColor(int n){
	int nth_band_color;
	switch(n){
	case 1:
		printColorMap();
		printf("\nPlease enter the number indicating the first color: ");
		scanf("%d",&nth_band_color);
		break;
	case 2:
		printColorMap();
		printf("\nPlease enter the number indicating the second color: ");
		scanf("%d",&nth_band_color);
		break;
	case 3:
		printColorMap();
		printf("\nPlease enter the number indicating the third color: ");
		scanf("%d",&nth_band_color);
		break;
	case 4:
		printColorMap();
		printf("\nPlease enter the number indicating the fourth color: ");
		scanf("%d",&nth_band_color);
		break;
	case 5:
		printColorMap();
		printf("\nPlease enter the number indicating the fifth color: ");
		scanf("%d",&nth_band_color);
		break;
	}
return nth_band_color;
}


int main()
{
	// declare variables for user input and result output
	int colors[5];
	int number;
	int resistance;
	int power;
	float tolerance;

	// prompt user for resistor type; control flow depends on this
	printf("\nDoes the resistor have 4 or 5 bands?:\n");
	scanf( "%d",&number );
	printf("input:%d\n",number);

	// 4 band resistor
	if(number==4){
		// prompt user for resistor band colors
		for(int i=1;i<5;i++){
			colors[i-1]=getColor(i);
			if(colors[i-1]==10){colors[i-1]=-1;}else if(colors[i-1]==11){colors[i-1]=-2;}
		}	
		
		// calculate 4 band resistance value
		resistance = ((colors[0]*10)+colors[1]);
		
		// calculate power (naturally maps to exponent value)
		power = colors[2];
		
		// map tolerance color key to actual value
		switch(colors[3]){
			case 1: // brown: 1%
				tolerance = 1.0;
				break;
			case 5: // green: 0.5%
				tolerance = 0.5;
				break;
			case 6: // blue: 0.25%
				tolerance = 0.25;
				break;
			case 7: // violet: 0.1%
				tolerance = 0.1;
				break;
			default:
				tolerance = 0.0;
				break;
		}

		// Output result
		printf("\n\nResistor Calculator believes you have an\n\n\t %d x 10^%d Ohm resistor with +/- %f percent tolerance\n\n",resistance,power,tolerance); 
	
	// 5 band resistor
	}else{
		// prompt user for resistor band colors
		for(int i=1;i<6;i++){
			colors[i-1]=getColor(i);
			if(colors[i-1]==10){colors[i-1]=-1;}else if(colors[i-1]==11){colors[i-1]=-2;}
		}	
		
		// calculate 5-band resistance value
		resistance = ((colors[0]*100)+colors[1]*10)+colors[2];
		
		// calculate exponent (10^power)
		power = colors[3];
		
		// map tolerance color key to actual value
		switch(colors[4]){
			case 1: // brown
				tolerance = 1.0;
				break;
			case 5: // green
				tolerance = 0.5;
				break;
			case 6: // blue
				tolerance = 0.25;
				break;
			case 7: // violet
				tolerance = 0.1;
				break;
			default:
				tolerance = 0.0;
				break;
		}

		// Output result
		printf("\n\nResistor Calculator believes you have an\n\n\t %d x 10^%d Ohm resistor with +/- %f percent tolerance\n\n",resistance,power,tolerance); 
	
	}
	return 0; 

}
