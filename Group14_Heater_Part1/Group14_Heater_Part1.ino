// Group 14 High Altitude Heater Part 1
// Dan Coen, Max Friedman &  Joseph Haaga
// 1/2/16

// Inputs (potentiometers)
// A0

// Outputs (LED pins)
// LED: 11
// Red: 3
// Green: 5
// Blue: 6

int sensorPin = A0;    // select the input pin for the potentiometer
int sensorValue = 0;  // variable to store the value coming from the sensor
int ledPin = 11; // RGB LED pin sensorValue
int temperature = 0; // temperature val
int pinVal = 0; 
#define REDPIN 3
#define GREENPIN 5
#define BLUEPIN 6
 

void setup() {
  // initialize serial comms
  Serial.begin(9600);
  // initialize pin 11 to output
  pinMode(ledPin, OUTPUT);
}


void loop() {
  /* RED: Range Value >= 950
    ORANGE: Range Value between 949 and 700
    YELLOW: Range Value between 699 and 400
    GREEN: Range Value between 399 and 300
    BLUE: Range Value is <= 199
  */
  // Send ~5V to LED
  digitalWrite(ledPin, 1);
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  // calculate analog voltage on A0
  float voltage = sensorValue * (5.0 / 1023.0);
  // LED Color logic
  if (sensorValue < 200) {
    Serial.print("LED is: Blue");
    analogWrite(REDPIN, 0);
    analogWrite(BLUEPIN, 255);
    analogWrite(GREENPIN, 0);
  } else if (sensorValue > 199 && sensorValue < 400) {
    Serial.print("LED is: Green");
    analogWrite(REDPIN, 0);
    analogWrite(BLUEPIN, 0);
    analogWrite(GREENPIN, 255);
  } else if (sensorValue > 400 && sensorValue < 600) {
    Serial.print("LED is: Yellow");
    analogWrite(REDPIN, 255);
    analogWrite(BLUEPIN, 0);
    analogWrite(GREENPIN, 255);
  } else if (sensorValue > 600 && sensorValue < 800) {
    Serial.print("LED is: Orange");
    analogWrite(REDPIN, 255);
    analogWrite(BLUEPIN, 0);
    analogWrite(GREENPIN, 165);
  } else if (sensorValue > 800) {
    Serial.print("LED is: Red");
    analogWrite(REDPIN, 255);
    analogWrite(BLUEPIN, 0);
    analogWrite(GREENPIN, 0);
  }

  // temperature logic (might need to make 750 into .75 for mV)
  Serial.print(voltage);
  Serial.print(" V\n");
  if (voltage == 750) {
    temperature = 25;
  } else {
    temperature = 25 + ((voltage - 750) / 10);
  }
  // print for debug
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.print("Celsius\n");
  // turn the ledPin on
  delay(500);
  // stop the program for <sensorValue> milliseconds:
  digitalWrite(ledPin, 0);
  delay(sensorValue);
  // turn the ledPin off:

  analogWrite(REDPIN, 0);
  analogWrite(BLUEPIN, 0);
  analogWrite(GREENPIN, 0);
  delay(500);
}

